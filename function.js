function sayHy(arr) {
  arr.forEach(i => {
    console.log("hy " + i.name);
  });
}

function sayBye(arr) {
  arr.forEach(i => {
    console.log("Bye " + i.name);
  });
}

module.exports = { sayHy, sayBye };
