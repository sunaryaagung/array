products = [
  {
    name: "white coffee",
    price: null
  },
  {
    name: "Rinso",
    price: 3000
  },
  {
    name: "Pena",
    price: 5000
  },
  {
    name: "Spidol",
    price: 10000
  },
  {
    name: null,
    price: null
  }
];

module.exports = products;
