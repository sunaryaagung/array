// cari umur
class Person {
  constructor(name, bornYear) {
    this.name = name;
    this.bornYear = bornYear;
  }

  //getter
  get myName() {
    return "My name is " + this.name;
  }

  get umur() {
    const date = new Date();
    const year = date.getFullYear();
    return year - this.bornYear;
  }

  //method
}
const zon = new Person("zon", 1995);
console.log(zon.myName + " and my age is " + zon.umur);
