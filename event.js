//Node js Events
const events = require("events");
const eventEmitter = new events.EventEmitter();

const person = require("./person");
const { sayHy, sayBye } = require("./function");

eventEmitter.on("hy", sayHy, person);
eventEmitter.on("bye", sayBye, person);

eventEmitter.emit("hy", person);

console.log(" ");

eventEmitter.emit("bye", person);
